## About
Simple, practical, and fast link unshortener.

### Install Python Modules
- On Debian Linux:
```bash
$ python3.11 -m pip install --break-system-packages --no-warn-script-location --user -r requirements.txt
```

- Other Linux:
```bash
$ python3.11 -m pip install --user -r requirements.txt
```

### How to use:
```bash
chmod +x unshorten_link.py
```

```bash
./unshorten_link.py --url [shortened link]
```

```bash
./unshorten_link.py --url https://encurtador.com.br/qtOS2
```
