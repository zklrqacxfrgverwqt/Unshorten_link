#!/usr/bin/env python3.11

import sys
try:
    import requests
except ImportError:
    sys.exit("Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user requests")
try:
    import argparse
    from argparse import RawTextHelpFormatter
except ImportError:
    sys.exit("Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user argparse")

parser = argparse.ArgumentParser(description="Link unshortener", 
                                 formatter_class=RawTextHelpFormatter, 
                                 epilog="Example:\n"
                                        f"    {sys.argv[0]} --url https://encurtador.com.br/qtOS2")
parser.add_argument("--url", type=str, required=True, help="Shortened link")
args = parser.parse_args()

user_agent = "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36"

def get_link(url, user_agent):
    headers = {'User-Agent': user_agent}
    try:
        response = requests.get(url, headers=headers, allow_redirects=True)
        response.raise_for_status()
# The original link will be in the last URL after the redirects
        original_link = response.url
        return original_link
    except requests.exceptions.RequestException as error:
        print(f"Error getting the original link: {error}")
        return None


if __name__ == "__main__":
    shortened_link = args.url
    original_link = get_link(shortened_link, user_agent)
    if original_link:
        print(f"Original link: {original_link}")
